<?php

function views_mviews_status() {
  $content = array();
  $content[] = views_mviews_header();
  $content[] = views_mviews_content();
  $t = views_mviews_status_table();
  $content[] = drupal_render($t);

  return implode("\n", $content);
}

function views_mviews_header() {
  return "<h1>Materializing Views</h1>";
}

function views_mviews_content() {
  return "This is a materialization plugin for Views. It creates temporary 
    materialized tables for Views.";
}

function views_mviews_status_table() {
  $build = array();
  $headers = array(
    t('View'),
    t('Status'),
    t('Indexed Rows'),
    // t('Total Rows'),
    t('Last Updated Time'),
  );
  
  $result = db_select('views_mviews', 'v')
      ->fields('v', array('view', 'status', 'indexed_rows', 'total_rows', 'last_updated_timestamp'))
      ->execute();
  
  $rows = array();
  foreach($result as $row) {
    $rows[] = array(
      $row->view,
      $row->status,
      $row->indexed_rows,
      // $row->total_rows,
      format_date($row->last_updated_timestamp, 'medium'),
    );
  }
  
  $build['status_table'] = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#empty' => t('No views materialized yet.'),
  );
  
  return $build; 
}