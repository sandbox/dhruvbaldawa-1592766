<?php

class ViewsMViewsHandler extends views_plugin_query_default {

  protected $flag = array();

  function init($base_table, $base_field, $options) {
    parent::init($base_table, $base_field, $options);
    $this->base_table = $base_table;
    $this->base_field = $base_field;
    $this->unpack_options($this->options, $options);
  }

  function plugin_title() {
    return 'Materialization of Views Plugin';
  }

  /**
   * Defines the options used by this query plugin.
   *
   * Adds an option to enable/disable materialization of a view.
   */
  public function option_definition() {
    return parent::option_definition() + array(
      'views_mviews_enable_materialization' => array(
        'default' => FALSE,
      ),
    );
  }

  /**
   * Add settings for the UI.
   *
   * Adds an option to enable/disable materialization of a view.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['views_mviews_enable_materialization'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Materialization'),
      '#description' => t('This option will enable materialization of the current view.'),
      '#default_value' => $this->options['views_mviews_enable_materialization'],
    );
  }

  public function execute(&$view, $flag = FALSE) {
    if (isset($view->query->options['views_mviews_enable_materialization']) &&
        $view->query->options['views_mviews_enable_materialization']) {

      // If the cloned view is passed.
      $mv = new MaterializedView($view);
      $status = $mv->get_status();
      if ($flag) {
        // Execute the cloned view.
        parent::execute($view);

        // Build the materialized view.
        $mv->build();
      }
      // If the original view is passed.
      else {
        // Create a clone and execute it, if its not indexed.
        if ($status != 'indexed') {
          $cloned_view = $view->clone_view();
          $cloned_view->init_display();
          // To get all the results from the database.
          $temp = $this->limit; // To restore back the limit.
          $this->limit = NULL;

          $cloned_view->init_query();
          // $cloned_view->init_pager();
          $cloned_view->build();
          // Execute the clone view.
          $this->execute($cloned_view, TRUE);

          $this->limit = $temp;
        }
        
        $query = $mv->get_query();
        foreach ($view->sort as $sort) {
          $this->add_orderby($sort->table, $sort->field, $sort->options['order']);
        }
        
        // @FIXME: Sort works, but very hacky, need to integrate with the query()
        // method, so that we can support other types. Also, inherit lots of 
        // methods from field handlers, so that there is enough support for other
        // features.
        // sorting works, but is very hacky
        if ($this->orderby) {
          foreach ($this->orderby as $order) {
            if ($order['field'] == 'rand_') {
              $query->orderRandom();
            }
            else {
              $query->orderBy($order['field'], $order['direction']);
            }
          }
        }
        
       $view->build_info['query'] = $query;

        // Execute the original view.
        parent::execute($view);
      }
    }
    else {
      parent::execute($view);
    }
  }

}