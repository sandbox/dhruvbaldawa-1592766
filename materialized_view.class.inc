<?php

// Design:
// $mv = new MaterializedView($view);
// MVQueue::register($mv); // This will register the mv with the queue
// The queue can keep track of the registered mv
// 
// Possible ways: any modification to $base_table or $participating_tables will
// mark the mv as "dirty" and just refresh that view.
//  * Refreshing can be done for a "dirty" when view is called.
//  * Incremental changes can also be applied, but look difficult.
//
class MaterializedView {

  protected $base_table;
  protected $participating_tables = array();
  protected $tables;
  protected $view;
  protected $fields;
  protected $name;
  protected $status;

  function __construct($view) {

    $this->base_table = $view->base_table;
    $this->tables = $view->build_info['query']->getTables();
//    dsm($view->build_info['query']->getFields());
    $this->view = &$view;
    $this->name = 'views_mviews_' . $this->view->name;
    $this->status = 'not indexed';
    $this->field_objects_map = array();
    // Enumerate fields
    if (!empty($this->view->field)) {
      foreach ($this->view->field as $key => $value) {
        $field_object = new MVFieldApiField($value);
        $this->fields[] = $field_object;
        $this->field_objects_map[$field_object->get_field_name()] = $field_object;
      }
    }
    else {
      foreach ($this->view->query->fields as $key => $value) {
        $field_object = new MVQueryField($value);
        $this->fields[] = $field_object;
        $this->field_objects_map[$field_object->get_field_name()] = $field_object;

      }
    }

    // Get all the participating tables
    foreach ($this->tables as $table) {
      $this->participating_tables[] = $table['table'];
    }

    $result = db_select('views_mviews', 'v')
        ->fields('v')
        ->condition('view', $this->view->name, '=')
        ->execute()
        ->fetchAssoc();

    if (empty($result)) {
      db_insert('views_mviews')
          ->fields(array(
            'view' => $this->view->name,
            'status' => 'not indexed',
          ))
          ->execute();
    }
    else {
      $this->status = $result['status'];
    }
  }

  /**
   * Method to return the name of the materialized view.
   * @return type 
   */
  function get_name() {
    return $this->name;
  }

  /**
   * Method to return the SelectQuery object to retrieve objects from the
   * database.
   * @return SelectQuery 
   */
  function get_query() {
    $select_query = db_select($this->name, 'v')->fields('v');
    //foreach ($sort as $this->view->sort)
    return $select_query;
  }

  /**
   * Method to get the required fields schema
   * @param type $name 
   */
  function get_fields_schema() {
    $field_schemas = array();
    foreach ($this->fields as $field) {
      $field_schemas = array_replace_recursive($field_schemas, $field->get_field_schema());
    }
    // dsm($field_schemas);
    return $field_schemas;
  }

  /**
   * Get the result of the particular materialized view.
   * @return type 
   */
  function get_view_result() {
    $result = array();
    // For views with 'field' attributes
    foreach ($this->view->result as $index => $value) {
      $result[$index] = array();
      foreach ($this->fields as $field) {
        $result[$index] = array_replace_recursive($result[$index], $field->get_field_result($value));
      }
    }
    return $result;
  }

  /**
   * Method will build the materialized view.
   * */
  function build() {
    // Design:
    // Use views_get_view_result to store it in a temporary table
    // Issue: whether to create separate table for (view, display) or just 
    // a view
    //
    $schema = array();
    $schema['description'] = 'Materialization table for ' . $this->view->human_name;
    // Get the schema for the fields required
    $schema['fields'] = $this->get_fields_schema();
    $schema['primary key'] = array($this->view->base_field);
    // Create the materialized view
    if (db_table_exists($this->name)) {
      $this->reset();
    }

    db_create_table($this->name, $schema);

    // Fill the data in the materialized view
    // dsm(views_get_view_result($this->view->name));
    $result_set = $this->get_view_result();
    foreach ($result_set as $row) {
      db_insert($this->name)->fields($row)->execute();
    }

    db_update('views_mviews')
        ->fields(array(
          'status' => 'indexed',
          'indexed_rows' => sizeof($result_set),
          'total_rows' => $this->view->query->pager->total_items,
          'last_updated_timestamp' => (int) time(),
        ))
        ->condition('view', $this->view->name, '=')
        ->execute();

    $this->status = 'indexed';
  }

  /**
   * Method will reset the materialized view.
   */
  function reset() {
    // Design:
    // Drop  the table
    db_drop_table($this->name);
    $this->status = 'not indexed';
  }

  /**
   * Method will refresh (reset and build) the materialized view. 
   */
  function refresh() {
    
  }

  /**
   * Method to get the current status of the materialized view. 
   */
  function get_status($reset = FALSE) {
    if ($reset) {
      $result = db_select('views_mviews', 'v')
          ->fields('v', array('status',))
          ->condition('view', $this->view->name, '=')
          ->execute()
          ->fetchCol();

      $this->status = $result[0];
    }
    return $this->status;
  }

  /**
   * Method to get the participating tables in a materialized view. 
   */
  function get_participating_tables() {
    return $this->participating_tables;
  }

  /**
   * Method to check if a table is participating in a materialized view or not. 
   */
  function is_participating($table) {
    return in_array($table, $this->participating_tables);
  }

  /**
   * Method to set a view as dirty. 
   */
  function set_dirty() {
    db_update('views_mviews')
        ->fields(array(
          'status' => 'dirty',
        ))
        ->condition('view', $this->view->name, '=')
        ->execute();

    $this->status = 'dirty';
  }

  /**
   * Method to check if a view is dirty or not. 
   */
  function is_dirty() {
    return $this->status;
  }
}

