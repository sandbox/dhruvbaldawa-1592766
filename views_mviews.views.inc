<?php

require_once('views_mviews.module');

/**
 * Implements hook_views_plugins() 
 */
function views_mviews_views_plugins() {
  return array(
    'query' => array(
      'views_mviews' => array(
        'title' => t('Materializing Views'),
        'help' => t('This plugin helps in materializing Views into temporary
          tables'),
        'handler' => 'ViewsMViewsHandler',
      ),
    ),
  );
}

/**
 * Implements hook_views_pre_execute() 
 */
function views_mviews_views_post_execute(&$view) {
}

/**
 * Implements hook_views_data_alter()
 */
function views_mviews_views_data_alter(&$data) {
  // Enable views_mviews as the query handler
  foreach ($data as &$table) {
    if (isset($table['table']['base']) &&
        (!isset($table['table']['base']['query class']) ||
        $table['table']['base']['query class'] == 'views_plugin_query_default')) {
      $table['table']['base']['query class'] = 'views_mviews';
    }
  }
}

?>
