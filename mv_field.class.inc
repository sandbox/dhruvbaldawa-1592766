<?php

/**
 * This class will act as the parent class to all the field classes. 
 */
class MVField {

  /**
   *
   * @var The name of the field. 
   */
  protected $name;

  /**
   *
   * @var The table to which the field belongs. 
   */
  protected $table;

  /**
   *
   * @var A boolean to see if the field has multiple values. 
   */
  protected $multiple_values;

  /**
   * @var The number of columns the field has. 
   */
  protected $n_columns;

  /**
   * @var The array containing name of all the columns.
   */
  protected $columns = array();

  /**
   * @var The array containing all the column and their schemas.
   */
  protected $field_schema = array();

  /**
   * returns an associative array with all the columns related to the field
   * and their corresponding schema. 
   */
  protected function get_field_schema() {
    
  }

  /**
   * returns the field value for a particular row. 
   */
  protected function get_field_result($row) {
    
  }

  /**
   * @param type $column_name Name of the column
   * @param type $column_schema The schema of the field
   */
  protected function _add_column($column_name, $column_schema, $column_field = NULL) {
    
  }

  /**
   * This method will build the field schema. 
   */
  protected function _build_schema() {
    
  }
  
  /**
   * This method is used to get the unique field name of the field. 
   */
  public function get_field_name() {
    
  }
}

class MVQueryField extends MVField {

  function __construct($field) {
    $this->name = $field['field'];
    $this->table = $field['table'];
    $this->alias = $field['alias'];
    $this->n_columns = 1;
    $this->multiple_values = FALSE;
    $this->columns = array();
    $this->field_schema = array();
    $this->_build_schema();
  }

  function _build_schema() {
    // Get the schema for the field.
    $schema = drupal_get_schema($this->table);
    $this->field_schema = array();
    $this->_add_column($this->alias, $schema['fields'], $this->name);
  }

  function _add_column($column_name, $column_schema, $column_field = NULL) {
    if (empty($column_field)) {
      $this->field_schema[$column_name] = $column_schema;
    }
    else {
      $this->field_schema[$column_name] = $column_schema[$column_field];
    }
    $this->columns[] = $column_name;

    // Change the 'serial' type to 'int'
    // @TODO: This information can be used to have composite primary keys.
    if ($this->field_schema[$column_name]['type'] == 'serial') {
      $this->field_schema[$column_name]['type'] = 'int';
    }
  }

  function get_field_schema() {
    // Build the schema if not built already.
    if (empty($this->field_schema)) {
      $this->_build_schema();
    }
    return $this->field_schema;
  }

  function get_field_result($row) {
    $return = array();
    $return[$this->alias] = $row->{$this->alias};

    return $return;
  }
  
  function get_field_name() {
    return $this->alias;
  }

}

class MVFieldApiField extends MVField {

  function __construct($field) {
    $this->name = $field->field;
    $this->table = $field->table;
    $this->alias = $field->field_alias;
    $this->field = $field;
    $this->columns = array();
    $this->field_columns = array();
    $this->field_schema = array();

    // Check for multi-valued fields.
    if (isset($field->multiple)) {
      $this->multiple_values = $field->multiple;
    }
    else {
      $this->multiple_values = FALSE;
    }

    // Check for number of columns.
    if (isset($field->field_info['columns'])) {
      // FIXME: Calculate the n_columns properly
      $this->n_columns = 1 + sizeof($field->field_info['columns']);
    }
    else {
      $this->n_columns = 1;
    }

    $this->_build_schema();
  }

  function _build_schema() {
    $this->field_schema = array();

    // Addition of additional fields
    if (count($this->field->additional_fields) > 0) {
      foreach ($this->field->additional_fields as $key => $value) {

        //FIXME: HACK !!! Dont know if its a good way to do 
        if (is_int($key)) {
          break;
        }

        if ($key == $this->field->view->base_field) {
          $field_name = $key;
        }
        else {
          $field_name = $this->field->table_alias . '_' . $key;
        }
        // If the table is given
        if (is_array($value)) {
          $schema = drupal_get_schema($value['table']);
          $this->_add_column($field_name, $schema['fields'], $value['field']);
        }
        // Else take the base table
        else {
          $schema = drupal_get_schema($this->table);
          $this->_add_column($field_name, $schema['fields'], $value);
        }
      }
    }

    // Additional columns for fields handled by views_handler_field_field
    if (!empty($this->field->field_info['columns'])) {
      foreach ($this->field->field_info['columns'] as $key => $value) {
        $this->_add_column($this->name . '_' . $key, $value);
        $this->field_columns[] = $this->name . '_' . $key;
      }
      return;
    }

    // FIXME: HACK !
    if (isset($this->field->base_table)) {
      $schema = drupal_get_schema($this->field->base_table);
    }
    else {
      $schema = drupal_get_schema($this->table);
    }
    // $field_schema[$this->alias] = $schema['fields'][$this->name];
    $this->_add_column($this->alias, $schema['fields'], $this->name);
  }

  function _add_column($column_name, $column_schema, $column_field = NULL) {
    if (empty($column_schema[$column_field])) {
      return;
    }
    
    if (isset($column_field)) {
      $this->field_schema[$column_name] = $column_schema[$column_field];
    }
    else {
      $this->field_schema[$column_name] = $column_schema;
    }
    $this->columns[] = $column_name;

    // Change the 'serial' type to 'int'
    // @TODO: This information can be used to have composite primary keys.
    if ($this->field_schema[$column_name]['type'] == 'serial') {
      $this->field_schema[$column_name]['type'] = 'int';
    }
  }

  function get_field_schema() {
    // Build the schema if not built already.
    if (empty($this->field_schema)) {
      $this->_build_schema();
    }
    return $this->field_schema;
  }

  function get_field_result($row) {
    $return = array();
    // Remove the $field->field_info['columns'] columns from the columns list
    $working_array = array_diff($this->columns, $this->field_columns);

    $field_result = $this->field->get_field($row);
    foreach ($working_array as $column) {
      if (isset($field_result->{$column})) {
        $return[$column] = $field_result->{$column};
      }
      else {
        $return[$column] = NULL;
      }
    }

    // FIXME: There can be a better implementation for this
    if (!empty($this->field->field_info['columns'])) {
      $field_value = $this->field->get_value($row);
      foreach ($this->field->field_info['columns'] as $key => $value) {
        $return[$this->name . '_' . $key] = $field_value[0][$key];
      }
    }
    return $return;
  }
  
  function get_field_name() {
    return $this->field->real_field;
  }

}