<?php

/**
 * @file
 * Database interface code for MySQL database servers for materializing views.
 */
/**
 * @ingroup database
 * @{
 */
require_once DRUPAL_ROOT . '/includes/database/mysql/database.inc';
require_once DRUPAL_ROOT . '/includes/database/database.inc';

class DatabaseConnection_mv_mysql extends DatabaseConnection_mysql {

  public function __construct(array $connection_options = array()) {
    parent::__construct($connection_options);
  }

  public function driver() {
    return 'mv_mysql';
  }

}

/**
 * @} End of "ingroup database".
 */
class SelectQuery_mv_mysql extends SelectQuery {
  public function __construct($table, $alias = NULL, DatabaseConnection $connection, $options = array()) {
    parent::__construct($table, $alias, $connection, $options);
  }
}