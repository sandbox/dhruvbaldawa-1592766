<?php

//
// Design:
// Will keep track of registered materialized views
// All updation, deletion and insertion jobs
// and will maintain the modification information
// will be invoked by database driver to check if the table is participating
// in any view, and then take necessary action
//
// @TODO: Decide on modification policies, and if the policies should be
// per-view basis or as a whole
class MVQueue {

  static private $deletion_jobs = array();
  static private $insertion_jobs = array();
  static private $updation_jobs = array();
  static private $materialized_views = array();
  static private $semaphore = 0; // semaphore to break recursion while querying.

  /**
   * This method will register the materialized view to the job queue.
   * @param MaterializedView $materialized_view 
   */
  public static function register($materialized_view) {
    // FIXME: A better way to get materialized view objects, rather than just 
    // querying. We might not require this method after all, only the database
    // table could be used to keep track of the materialized views and their
    // status
  }

  /**
   * This method will keep track of all the deletion operations.
   * @param DeleteQuery $object
   * @return type 
   */
  public static function delete($object) {
    self::check_views_consistency($object);
  }

  /**
   * This method will keep track of all the insertion operations.
   * @param InsertQuery $object
   * @return type 
   */
  public static function insert($object) {
    self::check_views_consistency($object);
  }

  /**
   * This method will keep track of all the updation operations.
   * @param UpdateQuery $object
   * @return type 
   */
  public static function update($object) {
    self::check_views_consistency($object);
  }

  /**
   * This method sets views who are participating in the event as dirty or not
   * dirty. 
   */
  public static function check_views_consistency($object) {
    // return if drupal has not bootstrapped completely
    if (drupal_get_bootstrap_phase() != DRUPAL_BOOTSTRAP_FULL) {
      return;
    }
    if (self::$semaphore > 0) {
      return;
    }
    self::$semaphore += 1;
    
    $dirty_views = self::get_views_using($object->get_table());
    if (empty($dirty_views)) {
      return;
    }
    else {
      foreach ($dirty_views as $dirty_view) {
        $dirty_view->set_dirty();
      }
    }
    
    self::$semaphore -= 1;
  }

  /**
   * This method will return whether a particular table is involved in any of
   * the materialized views or not.
   * @param string $table 
   */
  public static function get_views_using($table, $reset = FALSE) {
    $mviews = self::get_materialized_views($reset);
    $return = array();

    foreach ($mviews as $mview) {
      if (in_array($table, $mview->get_participating_tables())) {
        $return[] = $mview;
      }
    }

    return $return;
  }

  /**
   * This method will return the current available materialized view.
   * @return MaterializedView object 
   */
  public static function get_materialized_views($reset = FALSE) {
    // If not reset and materialized views already present, return it.
    if (!$reset && !empty(self::$materialized_views)) {
      return self::$materialized_views;
    }

    // Clear this stuff.
    self::$materialized_views = array();

    // Build materialized views objects.
    if (function_exists('views_get_enabled_views')) {
      $views = views_get_enabled_views();
    }
    else {
      return array();
    }
    foreach ($views as $view) {
      // Only include those views with materialization enabled.
      // @FIXME: Builds every view over here.
      $view->init_display();
      $view->init_query();
      $view->build();
      if (isset($view->query->options['views_mviews_enable_materialization']) &&
          $view->query->options['views_mviews_enable_materialization']) {
        self::$materialized_views[] = new MaterializedView($view);
      }
    }
    return self::$materialized_views;
  }

}

?>
