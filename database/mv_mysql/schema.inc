<?php

/**
 * @file
 * Database schema code for MySQL database servers for materializing views.
 */


/**
 * @ingroup schemaapi
 * @{
 */
require_once DRUPAL_ROOT . '/includes/database/mysql/schema.inc';
class DatabaseSchema_mv_mysql extends DatabaseSchema_mysql {
}

/**
 * @} End of "ingroup schemaapi".
 */
