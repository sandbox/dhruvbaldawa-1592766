<?php

/**
 * @file
 * Installation code for MySQL embedded database engine for materializing views.
 */

/**
 * Specifies installation tasks for MySQL and equivalent databases.
 */
require_once DRUPAL_ROOT . '/includes/database/mysql/install.inc';
class DatabaseTasks_mv_mysql extends DatabaseTasks_mysql {
}

