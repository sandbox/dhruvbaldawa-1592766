<?php

/**
 * @ingroup database
 * @{
 */

/**
 * @file
 * Query code for MySQL embedded database engine for materializing views.
 */

require_once DRUPAL_ROOT . '/includes/database/mysql/query.inc';
require_once DRUPAL_ROOT . '/includes/database/query.inc';
require_once 'MVQueue.class.inc'; // TODO: take this out of mv_mysql directory.
class InsertQuery_mv_mysql extends InsertQuery_mysql {
  function execute() {
    MVQueue::insert($this);
    return parent::execute();
  }
  
  function get_table() {
    return $this->table;
  }
  
}

class TruncateQuery_mv_mysql extends TruncateQuery_mysql {
  function execute() {
    MVQueue::delete($this);
    return parent::execute();
  }
  
  function get_table() {
    return $this->table;
  }
  
}

class DeleteQuery_mv_mysql extends DeleteQuery {
  function execute() {
    MVQueue::delete($this); // Testing !
    return parent::execute();
  }
  
  function get_table() {
    return $this->table;
  }
}

class UpdateQuery_mv_mysql extends UpdateQuery {
  function execute() {
    MVQueue::update($this);
    return parent::execute();
  }
  
  function get_table() {
    return $this->table;
  }
}
/**
 * @} End of "ingroup database".
 */
